
CREATE  TABLE course
  (name VARCHAR(50),
   credits INTEGER);

CREATE TABLE  schedule
  (studentID INTEGER,
   offeringId INTEGER);

CREATE TABLE  offering
  (id INTEGER,
   name VARCHAR(50),
   daysTimes VARCHAR(100));

CREATE TABLE student
(id INTEGER,
name VARCHAR(60));
