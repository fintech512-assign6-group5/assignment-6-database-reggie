public interface Entry<T> {
    public T GetKey();
}
