import java.util.Objects;

public final class OfferingEntry implements Entry<Integer> {
    private int id;
    private final Course course;
    private final String daysTimes;

    public OfferingEntry(Course course, String daysTimes) {
        this.course = course;
        this.daysTimes = daysTimes;
    }

    public Course course() {
        return course;
    }

    public int id() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String daysTimes() {
        return daysTimes;
    }

    @Override
    public Integer GetKey() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (OfferingEntry) obj;
        return this.id == that.id &&
                Objects.equals(this.course, that.course) &&
                Objects.equals(this.daysTimes, that.daysTimes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, course, daysTimes);
    }

    @Override
    public String toString() {
        return "OfferingEntry[" +
                "id=" + id + ", " +
                "course=" + course + ", " +
                "daysTimes=" + daysTimes + ']';
    }


}
