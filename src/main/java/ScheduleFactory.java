import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class ScheduleFactory extends Factory<String> {

    private static ScheduleFactory _instance;

    public static ScheduleFactory Instance() {
        if (ScheduleFactory._instance == null) {
            _instance = new ScheduleFactory();
        }
        return _instance;
    }

    public ScheduleFactory() {
        tableName = "schedule";
        keyName = "name";
    }

    SQLObject CreateBody(Statement statement, Entry<String> entry) throws SQLException {
        ScheduleEntry scheduleEntry = (ScheduleEntry) entry;
        statement.executeUpdate(AssembleDeleteByKey(entry));
        return new Schedule(scheduleEntry.name());
    }

    @Override
    SQLObject FindBody(Statement statement, String key) throws SQLException {
        String strKey = "'" + key + "'";
        ResultSet result = statement.executeQuery(AssembleSelect(strKey));
        Schedule schedule = new Schedule(key);
        while (result.next()) {
            int offeringId = result.getInt("offeringId");
            Offering offering = Offering.find(offeringId);
            schedule.add(offering);
        }
        return schedule;
    }

    @Override
    public void UpdateBody(Statement statement, Entry<String> entry) throws SQLException {
        ScheduleEntry scheduleEntry = (ScheduleEntry) entry;
        statement.executeUpdate(AssembleDeleteByKey(entry));
        for (int i = 0; i < scheduleEntry.schedule().size(); i++) {
            Offering offering = scheduleEntry.schedule().get(i);
            statement.executeUpdate(AssembleInsert(entry, offering.getId()));
        }
    }

    public void DeleteAll() {
        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            Statement statement = conn.createStatement();
            DeleteBody(statement);
        } catch (Exception ignored) {
        }
    }

    public void DeleteBody(Statement statement) throws SQLException {
        statement.executeUpdate("DELETE * FROM schedule");
    }

    public Collection<Schedule> All() {
        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            Statement statement = conn.createStatement();
            return AllBody(statement);
        } catch (Exception ignored) {
            return null;
        }
    }

    public Collection<Schedule> AllBody(Statement statement) throws SQLException {
        ArrayList<Schedule> result = new ArrayList<Schedule>();
        ResultSet results = statement.executeQuery(AssembleSelectDistinctName());
        while (results.next())
            result.add(Schedule.find(results.getString("name")));
        return result;
    }

    public java.lang.String AssembleInsert(Entry<String> entry, int id) {
        //INSERT INTO schedule VALUES(name, id)
        ScheduleEntry scheduleEntry = (ScheduleEntry) entry;
        return String.format("INSERT INTO %s VALUES('%s', %s)",
                tableName,
                scheduleEntry.GetKey(),
                id);
    }

    public String AssembleSelectDistinctName(){
        return String.format("SELECT DISTINCT name FROM %s ORDER BY name", tableName);
    }
}
