import jdk.jshell.spi.ExecutionControl;

import java.sql.*;

public abstract class Factory<T> {

    static String dbName="reggie";
    static String connectionURL = "jdbc:derby:" + dbName + ";create=true";
    protected String tableName;
    protected String keyName;

    public SQLObject Create(Entry<T> entry) {
        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            Statement statement = conn.createStatement();
            return CreateBody(statement, entry);
        } catch (Exception ex) {
            return null;
        }
    }

    public SQLObject Find(T key) {
        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            Statement statement = conn.createStatement();
            return FindBody(statement, key);
        } catch (Exception ex) {
            return null;
        }
    }

    public void Update(Entry<T> entry) {
        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            Statement statement = conn.createStatement();
            UpdateBody(statement, entry);
        } catch (Exception ignored) {
        }
    }

    abstract SQLObject CreateBody(Statement statement, Entry<T> entry) throws SQLException;
    abstract SQLObject FindBody(Statement statement, T key) throws SQLException;
    abstract void UpdateBody(Statement statement, Entry<T> entry) throws SQLException;

    public String AssembleSelect(T key) {
        return "SELECT * FROM "+ tableName + " WHERE "+ keyName +" = " + key;
    }

    public String AssembleDeleteByKey(Entry<T> entry) {
        String formatStr;
        if (entry.GetKey() instanceof Integer) {
            formatStr = "DELETE FROM %s WHERE %s = %s";
        } else {
            formatStr = "DELETE FROM %s WHERE %s = '%s'";
        }
        return String.format(formatStr,
                tableName,
                keyName,
                entry.GetKey());
    }

    public String AssembleInsert(Entry<T> entry) throws ExecutionControl.NotImplementedException{
        throw new ExecutionControl.NotImplementedException("AssembleInsert");
    }


}

