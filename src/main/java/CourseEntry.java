public record CourseEntry(String name, int credits) implements Entry<String> {

    @Override
    public String name() {
        return name;
    }

    @Override
    public int credits() {
        return credits;
    }

    @Override
    public String GetKey() {
        return name;
    }

}
