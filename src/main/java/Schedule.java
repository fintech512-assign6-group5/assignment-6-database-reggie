import java.util.*;
import java.sql.*;

public class Schedule extends SQLObject {

	static ScheduleFactory factory = ScheduleFactory.Instance();

	final private ScheduleEntry entry;
	int credits = 0;
	static final int minCredits = 12;
	static final int maxCredits = 18;
	boolean overloadAuthorized = false;

	static String dbName="reggie";
	static String connectionURL = "jdbc:derby:" + dbName + ";create=true";


	public static void deleteAll() {
		factory.DeleteAll();
	}

	public static Schedule create(ScheduleEntry entry) {
		return (Schedule) factory.Create(entry);
	}

	public static Schedule find(String name) {
		return (Schedule) factory.Find(name);
	}

	public static Collection<Schedule> all() throws Exception {
		return factory.All();
	}

	public void update() {
		factory.Update(entry);
	}

	public Schedule(String name) {
		entry = new ScheduleEntry(name, new ArrayList<>());
	}

	public void add(Offering offering) {
		credits += offering.getCourse().getCredits();
		entry.schedule().add(offering);
	}

	public void authorizeOverload(boolean authorized) {
		overloadAuthorized = authorized;
	}

	public List<String> analysis() {
		ArrayList<String> result = new ArrayList<String>();

		if (credits < minCredits)
			result.add("Too few credits");

		if (credits > maxCredits && !overloadAuthorized)
			result.add("Too many credits");

		checkDuplicateCourses(result);

		checkOverlap(result);

		return result;
	}

	public void checkDuplicateCourses(ArrayList<String> analysis) {
		HashSet<Course> courses = new HashSet<>();
		for (int i = 0; i < entry.schedule().size(); i++) {
			Course course = (entry.schedule().get(i)).getCourse();
			if (courses.contains(course))
				analysis.add("Same course twice - " + course.getName());
			courses.add(course);
		}
	}

	public void checkOverlap(ArrayList<String> analysis) {
		HashSet<String> times = new HashSet<String>();
		for (Offering offering : entry.schedule()) {
			String daysTimes = offering.getDaysTimes();
			StringTokenizer tokens = new StringTokenizer(daysTimes, ",");
			while (tokens.hasMoreTokens()) {
				String dayTime = tokens.nextToken();
				if (times.contains(dayTime))
					analysis.add("Course overlap - " + dayTime);
				times.add(dayTime);
			}
		}
	}

	public String getName() {
		return entry.name();
	}

	public ArrayList<Offering> getSchedule() {
		return entry.schedule();
	}

	public String toString() {
		return "Schedule " + getName() + ": " + getSchedule();
	}
}
