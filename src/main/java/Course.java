public class Course extends SQLObject {
	final private CourseEntry entry;
	static CourseFactory factory = CourseFactory.Instance();

	public static Course create(CourseEntry entry) {
		return (Course) factory.Create(entry);
	}

	public static Course find(String name) {
		return (Course) factory.Find(name);
	}

	public void update() {
		factory.Update(entry);
	}

	public Course(String name, int credits) {
		entry = new CourseEntry(name, credits);
	}

	public int getCredits() {
		return entry.credits();
	}

	public String getName() {
		return entry.name();
	}
}
