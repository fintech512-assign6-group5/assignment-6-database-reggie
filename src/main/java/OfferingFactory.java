import java.sql.*;

public class OfferingFactory extends Factory<Integer> {

    private static OfferingFactory _instance;

    public static OfferingFactory Instance() {
        if (OfferingFactory._instance == null) {
            _instance = new OfferingFactory();
        }
        return _instance;
    }

    public OfferingFactory() {
        tableName = "offering";
        keyName = "id";
    }

    SQLObject CreateBody(Statement statement, Entry<Integer> entry) throws SQLException {
        OfferingEntry offeringEntry = (OfferingEntry) entry;
        ResultSet result = statement.executeQuery("SELECT MAX(id) FROM offering");
        result.next();
        int newId = 1 + result.getInt(1);
        statement.executeUpdate(AssembleInsert(entry));
        return new Offering(newId, offeringEntry.course(), offeringEntry.daysTimes());
    }

    @Override
    SQLObject FindBody(Statement statement, Integer key) throws SQLException {
        ResultSet result = statement.executeQuery(AssembleSelect(key));
        if (!result.next())
            return null;
        String courseName = result.getString("name");
        Course course = Course.find(courseName);
        String dateTime = result.getString("daysTimes");
        return new Offering(key, course, dateTime);
    }

    @Override
    public void UpdateBody(Statement statement, Entry<Integer> entry) throws SQLException {
        OfferingEntry offeringEntry = (OfferingEntry) entry;
        statement.executeUpdate(AssembleDeleteByKey(entry));
        statement.executeUpdate(AssembleInsert(entry));
    }

    @Override
    public java.lang.String AssembleInsert(Entry<Integer> entry) {
        // INSERT INTO offering VALUES(id, 'course', 'daysTimes')
        OfferingEntry offeringEntry = (OfferingEntry) entry;
        return String.format("INSERT INTO %s VALUES(%s, '%s', '%s')",
                tableName,
                offeringEntry.GetKey(),
                offeringEntry.course().getName(),
                offeringEntry.daysTimes());
    }

}
