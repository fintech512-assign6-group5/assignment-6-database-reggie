public class Offering extends SQLObject {
	final private OfferingEntry entry;
	static OfferingFactory factory = OfferingFactory.Instance();

	public static Offering create(OfferingEntry entry) {
		return (Offering) factory.Create(entry);
	}

	public static Offering find(int id) {
		return (Offering) factory.Find(id);
	}

	public void update() {
		 factory.Update(entry);
	}

	public Offering(int id, Course course, String daysTimesCsv) {
		entry = new OfferingEntry(course, daysTimesCsv);
		entry.setId(id);
	}

	public int getId() {
		return entry.id();
	}

	public Course getCourse() {
		return entry.course();
	}

	public String getDaysTimes() {
		return entry.daysTimes();
	}

	public String toString() {
		return "Offering " + getId() + ": " + getCourse() + " meeting " + getDaysTimes();
	}
}
