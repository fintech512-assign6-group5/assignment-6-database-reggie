import java.sql.*;

public class CourseFactory extends Factory<String> {

    private static CourseFactory _instance;

    public static CourseFactory Instance() {
        if (CourseFactory._instance == null) {
            _instance = new CourseFactory();
        }
        return _instance;
    }

    public CourseFactory() {
        tableName = "course";
        keyName = "name";
    }

    @Override
    SQLObject CreateBody(Statement statement, Entry<String> entry) throws SQLException {
        CourseEntry courseEntry = (CourseEntry) entry;
        statement.executeUpdate(AssembleDeleteByKey(entry));
        statement.executeUpdate(AssembleInsert(entry));
        return new Course(courseEntry.name(), courseEntry.credits());
    }

    @Override
    SQLObject FindBody(Statement statement, String key) throws SQLException {
        String strKey = "'" + key + "'";
        ResultSet result = statement.executeQuery(AssembleSelect(strKey));
        if (!result.next())
            return null;

        int credits = result.getInt("Credits");
        return new Course(key, credits);
    }

    @Override
    public void UpdateBody(Statement statement, Entry<String> entry) throws SQLException {
        statement.executeUpdate(AssembleDeleteByKey(entry));
        statement.executeUpdate(AssembleInsert(entry));
    }

    @Override
    public String AssembleInsert(Entry<String> entry) {
        // INSERT INTO course VALUES('name', credit)
        CourseEntry courseEntry = (CourseEntry) entry;
        return String.format("INSERT INTO %s VALUES('%s', %s)",
                tableName,
                courseEntry.GetKey(),
                courseEntry.credits());
    }
}
