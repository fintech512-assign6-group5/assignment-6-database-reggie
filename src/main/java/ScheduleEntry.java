import java.util.ArrayList;

public record ScheduleEntry(String name, ArrayList<Offering> schedule) implements Entry<String> {

    @Override
    public String name() {
        return name;
    }

    @Override
    public ArrayList<Offering> schedule() {
        return schedule;
    }

    @Override
    public String GetKey() {
        return name;
    }

}
