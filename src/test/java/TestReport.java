import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;

public class TestReport {
    public static String COURSE_CREATE="CREATE TABLE course (name VARCHAR(50),  credits INT)";

    public static String SCHEDULE_CREATE="""
                                     CREATE TABLE schedule
                                         (name VARCHAR(100),  offeringId INT)
                                  """;

    public static String OFFERING_CREATE="""
                                         CREATE TABLE offering
                                           (id INT,   name VARCHAR(50),  daysTimes VARCHAR(100))
                                  """;


    @BeforeAll
    public static void initializeDatabase() {
        Path pathToBeDeleted = Path.of("reggie");

        try {
            Files.walk(pathToBeDeleted)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            if (!(e instanceof java.nio.file.NoSuchFileException)) {
               fail("Unable to delete existing database: "+e);
            }
        }


        String driver = "org.apache.derby.jdbc.EmbeddedDriver";
        String dbName="reggie";
        String connectionURL = "jdbc:derby:" + dbName + ";create=true";
        try {
            Connection conn = DriverManager.getConnection(connectionURL);
            Statement stmt = conn.createStatement();
            stmt.execute(COURSE_CREATE);
            stmt.execute(SCHEDULE_CREATE);
            stmt.execute(OFFERING_CREATE);
        }
        catch (SQLException e) {
            fail("Error initialization database: "+e);
        }
    }

    @Test
    public void testEmptyReport() throws Exception {
        Schedule.deleteAll();
        Report report = new Report();

        StringBuilder buffer = new StringBuilder();

        report.write(buffer);

        assertEquals("Number of scheduled offerings: 0\n", buffer.toString());
    }

	@Test
    public void testReport() throws Exception {
        Schedule.deleteAll();

        CourseEntry cs1011 = new CourseEntry("CS101", 3);
        Course cs101 = Course.create(cs1011);
        cs101.update();
        Offering off1 = Offering.create(new OfferingEntry(cs101, "M10"));
        off1.update();
        Offering off2 = Offering.create(new OfferingEntry(cs101, "T9"));
        off2.update();

        Schedule s = Schedule.create(new ScheduleEntry("Bob", new ArrayList<>()));
        s.add(off1);
        s.add(off2);
        s.update();

        Schedule s2 = Schedule.create(new ScheduleEntry("Alice", new ArrayList<>()));
        s2.add(off1);
        s2.update();

        Report report = new Report();

        StringBuilder sb = new StringBuilder();

        report.write(sb);

        String result = sb.toString();
        String valid1 = "CS101 M10\n\tAlice\n\tBob\n" +
                        "CS101 T9\n\tBob\n" +
                        "Number of scheduled offerings: 2\n";

        String valid2 = "CS101 T9\n\tBob\n" +
                        "CS101 M10\n\tAlice\n\tBob\n" +
                        "Number of scheduled offerings: 2\n";
        
        assertTrue(result.equals(valid1) || result.equals(valid2));
    }
}

